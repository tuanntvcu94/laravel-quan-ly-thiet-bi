<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('about', function(){
	return 'About Content';
});
Route::get('about/directions', function(){
	return 'directions go here';
});
Route::any('Submit-form',function(){
	return 'Process form';
});
Route::get('about/{theSubject}',function ($theSubject){
	return $theSubject.'content go here';
});
Route::get('about/classes/{theSubject}',function ($theSubject){
	return "content on $theSubject";
});
Route::get('about/classes/{theArt}/{thePrice}',function ($theArt,$thePrice){
	return "the product :$theArt and $thePrice";
});
Route::get('profile/{name}','ProfileController@showProfile');
Auth::routes();
Route::group(['prefix'=>'admin'],function(){

    Route::get('/', 'HomeController@index')->name('dashboard');

    Route::group(['prefix' => 'device'], function() {
        Route::get('/', 'DeviceController@deviceList')->name('list-device');
        Route::get('/form/{id?}', 'DeviceController@getForm')->where('id', '[0-9]+');
        Route::post('/form/{id?}', 'DeviceController@submitForm')->where('id', '[0-9]+');
        Route::get('/list', 'DeviceController@deviceList')->middleware('auth')->name('list-device');
        Route::get('/delete/{deviceId}', 'DeviceController@deleteDevice')->middleware('auth');
    });

	Route::group(['prefix'=>'employee'],function(){
		Route::get('/','EmployeeController@getList')->name('list-employee');
		// Route::post('List','EmployeeController@postList');

		Route::get('edit/{id}','EmployeeController@getEdit')->where('id', '[0-9]+')->name('edit-employee');
		Route::post('edit/{id}','EmployeeController@postEdit')->where('id', '[0-9]+');

		Route::get('form','EmployeeController@getAdd')->name('add-employee');
		Route::post('form','EmployeeController@postAdd');

		Route::get('delete/{id}','EmployeeController@getDelete')->name('delete-employee');

		Route::get('file','EmployeeController@getFile');

		
	});
});