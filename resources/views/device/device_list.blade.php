@extends('adminlte::page')

@section('content_header')
  <div class="row">
    <div class="col-xs-6">
        <h1>Device List</h1>
    </div>
    <div class="col-xs-6 text-right">
        <a
        href="{{ url('admin/device/form/0')}}"
        class="btn btn-success btn-sm"
        style="margin-top: 30px;"
        >Thêm mới</a>
    </div>
  </div>
@stop

@section('content')
<table class="table table-striped w-auto">
  <!--Table head-->
  <thead>
    <tr>
      <th class="text-center">#</th>
      <th class="text-center">device_name</th>
      <th class="text-center">employee_name</th>
      <th class="text-center">department</th>
      <th class="text-center">amount</th>
      <th class="text-center">status</th>
      <th class="text-center">Actions</th>
    </tr>
    </thead>
    <!--Table head-->
      @foreach ($devices as $key => $device)
       <!--Table body-->
       
    <tbody>
      <tr class="table-info text-center">
        <th scope="row">{{ $key+1 }}</th>
        <td>{{ $device->device_name }}</td>
        <td>{{ $device->employee->employee_name??'-' }}</td>
        <td>{{ $device->employee->department??'-' }}</td>
        <td>{{ $device->amount }}</td>
        <td>{{ $device->status == 1 ? "Good" : "Bad"  }}</td>
        <td>
          <a href="{{ url('device/form/'.$device->id)}}" class="btn btn-primary btn-xs" role="button">
            <i class="fa fa-edit"></i>
          </a>
          <a href="{{ url('admin/device/delete/'.$device->id) }}" class="btn btn-danger btn-xs" role="button">
            <i class="fa fa-trash"></i>
          </a>
        </td>
      </tr>
    </tbody>

    <tfoot>
      <tr>
        <td colspan="8" class="text-right">
          {{ $devices->links() }}
        </td>
      </tr>
    </tfoot>
    
    <!--Table body-->
      @endforeach
  <!--Table-->
  </table>
  <!--Table-->
      
@stop
