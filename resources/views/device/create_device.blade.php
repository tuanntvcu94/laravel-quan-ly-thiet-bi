
@extends('adminlte::page')

@section('content_header')
    <h3>Device</h3>
@stop

@section('content')
  <div class="container">
    <div class="row">
     <div class="col-sm-5">
        <form method='POST'>
        {{csrf_field()}}
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
        @endif

        <div class="form-group">
            <label for="formGroupExampleInput" >Device name:</label>
            <input type="text" class="form-control form-control-sm" 
                   id="formGroupExampleInput" name='device_name' 
                   value="{{$data->device_name??''}}" placeholder="Please Enter Device Name" >
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput2">Employee information</label>
            <select class="form-control form-control-sm" name="employee_id">
                @foreach ($employees as $employee)
                    <option
                    value="{{$employee->id}}"
                    {{$data&&$data->employee_id == $employee->id ? 'selected' : ''}}
                    >{{$employee->employee_name}} - {{$employee->department}} </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput2">Amount</label>
            <input type="number" min="0" class="form-control form-control-sm" id="formGroupExampleInput2"
                   name='amount' value="{{$data->amount??''}}"
                   placeholder="Please Enter Amount" >
        </div>
        <div>
            <label for="formGroupExampleInput2">Status</label>
            <select class="form-control form-control-sm" name="status">
            <option value="1" 
                    {{$data && $data->status == 1 ? 'selected' : ''}}> Good </option>
            <option value="2"
                    {{$data && $data->status == 2 ? 'selected' : ''}}> Bad </option>
            </select>
        </div>
        <div class='mt-4'>
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
   </div>
  </div>
@stop
