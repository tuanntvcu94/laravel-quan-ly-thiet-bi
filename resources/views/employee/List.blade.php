@extends('adminlte::page')
@section('content_header')

    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">

                    <h1 class="page-header">EMPLOYEE
                        <small>LIST</small>
                    </h1>
                </div>
                <!-- /.col-lg-12 -->
                @if(session('thong bao'))
                    <div class="alert alert-success">
                        {{session('success')}}
                    </div>
                @endif
                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                    <thead>
                    <tr class="text-center">
                        <th>#</th>
                        <th>Employee_name</th>
                        <th>Department</th>
                        <th>Date of birth</th>
                        <th>Avatar</th>
                        <th>Delete</th>
                        <th>Edit</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($employees as $key => $nv)
                        <tr class="odd gradeX text-center">
                            <td>{{$key+1}}</td>
                            <td>{{$nv->employee_name}}</td>
                            <td>{{$nv->department}}</td>
                            <td>{{$nv->age}}</td>
                            <td>
                                <div style="width: 50px ; 
                                            height: 50px">
                                    <img src="{{asset($nv->avatar)}}" class="img-responsive"/>
                                </div>
                            </td>
                            <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a
                                        href="{{ route('delete-employee', ['id' => $nv->id]) }}"> Delete</a></td>
                            <td class="center">
                                <i class="fa fa-pencil fa-fw"></i> <a
                                        href="{{ route('edit-employee', ['id' => $nv->id]) }}">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

@endsection