@extends('adminlte::page')
@section('content_header')

<!-- Page Content -->
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">EMPLOYEE
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                      @if(session('thong bao'))
                            <div class="alert alert-success">
                                   {{session('thong bao')}}
                            </div>
                        @endif  
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form action="{{ route('add-employee') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            
                            <div class="form-group">
                                <label>Employee Name</label>
                                <input class="form-control" name="employee_name" placeholder="Please Enter Employee  Name" />
                            </div>

                            <div class="form-group">
                                <label>Department</label>
                                <input class="form-control" name="department" placeholder="Please Enter department"/>
                            </div>

                           
                            <div class="form-group">
                                <label>Birthday</label>
                                <input type="date" name="age" class="form-control"/>
                            </div>


                            <div class="form-group">
                                <label>Avatar</label>
                                <input type="file" name="avatar" accept="image/*"/>
                            </div>
                                                    
                            <button type="submit" class="btn btn-primary">Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
 </div>
        <!-- /#page-wrapper -->
@endsection
        