@extends('adminlte::page')

@section('content_header')
<!-- Page Content -->
<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">EMPLOYEE
                            <small>{{$employees->employee_name}}</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        @if(count($errors) > 0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                    {{$err}}<br>
                                @endforeach
                             </div>
                        @endif
                        
                        @if(session('thong bao'))
                            <div class="alert alert-success">
                                   {{session('thong bao')}}
                            </div>
                        @endif    
                        <form action="{{route('edit-employee', ['id'=>$employees->id])}}" method="POST">
                            @csrf
                           
                            <div class="form-group">
                                <label>EMPLOYEE Name</label>
                                <input required class="form-control" name="employee_name" placeholder="Please Enter EMPLOYEE Name" value="{{$employees->employee_name}}" />
                            </div>

                            <div class="form-group">
                                <label>Department</label>
                                <input required class="form-control" name="department" placeholder="Please Enter Department" value="{{$employees->department}}" />
                            </div>

                             <div class="form-group">
                                <label>AGE</label>
                                <input required type="date" name="age" value="{{$employees->age}}"  />
                            </div>
                        
                           
                            <button type="submit" class="btn btn-default" >Edit</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

@endsection