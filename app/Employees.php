<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class employees extends Model
{   use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'employees';
    protected $fillable = ['id','employee_name','department','age','avatar'];
    public $timestamps = false;
}
