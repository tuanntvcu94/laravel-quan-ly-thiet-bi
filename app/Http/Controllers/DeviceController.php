<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;
use App\Employee;

class DeviceController extends Controller
{
    public function deviceList()
    {
        return view(
            'device/device_list',
            [
                'devices' => Device::paginate(env('PAGE_SIZE', 10))
            ]
        );
    }

    protected function getForm($id = 0)
    {
        $employees = Employee::all();
        $data = null;
        if ($id > 0) {
            // query details
            $data = Device::find($id);
        }
        return view('device/create_device', compact(['data', 'employees']));
    }

    protected function submitForm(Request $request, $id = 0)
    {
        $employees = Employee::all();
        $request->validate([
            'device_name' => 'string|required',
            'amount' => 'required|integer|min:0'
        ]);
        if ($id > 0) {
            // query details
            $data = Device::find($id);
            $employees = Employee::all();
        } else {
            $data = new Device;
        }
        $data->employee_id = $request->employee_id;
        $data->device_name = $request->device_name;
        $data->amount = $request->amount;
        $data->status = $request->status;
        if ($data->save()) {
            session()->flash('msg', '');
            return redirect()->route('list-device');
        } else {
            session()->flash('error', '');
        }
        return view('create_device', compact(['data', 'employees']));
    }

    public function deleteDevice(Request $request, $deviceId)
    {
        Device::whereId($deviceId)->delete();
        return redirect()->route('list-device');
    }
}