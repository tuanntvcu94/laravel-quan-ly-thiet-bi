<?php

namespace App\Http\Controllers;

use Input;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Employee;

class EmployeeController extends Controller
{
    public function getList()
    {
        $employees = Employee::all();

        return view('employee/List', ['employees' => $employees]);
    }

    protected function getAdd()
    {
        $employees = Employee::all();
        return view('employee/Add', ['employees' => $employees]);
    }

    /** todo: Save Employee
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function postAdd(Request $request)
    {
        $this->validate($request,
            [
                'employee_name' => 'required|string|min:3|max:50',
                'department'=> 'required|string|min:3|max:50',
                'age' => 'required|min:1|max:100',
                'avatar' => 'file'
            ],
            [
                'employee_name.required' => 'ban chua nhap ten nhan vien',
                'employee_name.unique' => 'ten da ton tai',
                'employee_name.min' => 'ten nhan vien phai co do dai tu 3 den 50 ki tu',
                'employee_name.max' => 'ten nhan vien phai co do dai tu 3 den 50 ki tu',
            ]
        );
        $employees = new Employee;
        $employees->employee_name = $request->employee_name;
        $employees->department = $request->department;
        $employees->age = $request->age;
        $file = $request->file('avatar');
        if ($file->isValid()) {
            $file_name = time() . '.' . $file->getClientOriginalExtension();
            $file->move(public_path('upload/avatar'), $file_name);
            $employees->avatar = 'upload/avatar/' . $file_name;
        }

        $employees->save();
        session()->flash('msg', 'Success');
        return back();
    }

    public function getEdit($id)
    {
        $employees = Employee::find($id);
        return view('employee/Edit', ['employees' => $employees]);

    }

    public function postEdit(Request $request, $id)

    {
        $employees = Employee::find($id);
        $this->validate($request,
            [
                'employee_name' => 'required|min:3|max:50',
                'department'=> 'required|string|min:3|max:50',
                'age' => 'required|min:1|max:100'
            ],
            [
                'employee_name.required' => 'ban chua nhap ten nhan vien',
                'employee_name.unique' => 'ten da ton tai',
                'employee_name.min' => 'ten nhan vien phai co do dai tu 3 den 50 ki tu',
                'employee_name.max' => 'ten nhan vien phai co do dai tu 3 den 50 ki tu',
            ]
        );

        $employees->employee_name = $request->employee_name;
        $employees->department = $request->department;
        $employees->age = $request->age;
        return $employees->save()? redirect(route('list-employee')) : back();
    }

    public function getDelete($id)
    {
        Employee::whereId($id)->delete();
        return redirect('admin/employee');


    }

    public function insertPhoto($fileName, $path, $defaultName = null, Request $request)
    {
        $photo = null;
        $file = Input::file($fileName);
        if (Input::hasFile($fileName)) {
            $destinationPath = $path;
            $extension = $file->getClientOriginalExtension();
            $name = $file->getClientOriginalName();
            $name = date('Y-m-d') . Time() . rand(11111, 99999) . '.' . $extension;
            $photo = $destinationPath . '/' . $name;
            $file->move($destinationPath, $name);
        } else {
            $photo = $defaultName;
        }
        return $photo;
    }

    public function postUpload(Request $request)
    {
        try {
            $data = array('image_path' => Photo::insertPhoto('image_path', '../resources/assets/imageUpload', 'no image', $request));
            DB::table('tblImage')->insert($data);

            return "saved";
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}

      
